# LearningToLearn

This is a repository of an implementation of Learning To Learn by Gradient descent by gradient descent and Optimization as a model for Few Shot Learning papers in both Tensorflow and PyTorch.
This project was part of the coursework 10-605 - Machine Learning for Large Datasets ICLR Reproducibility Challenge.

The actual git repository of the paper can be found [here](https://github.com/deepmind/learning-to-learn). The implementation provides scope for extension, where we can solve optimization problems for more machine learning algorithms. 
The [Few Shot Optimizer using LSTM](https://github.com/twitter/meta-learning-lstm) was inspired from this paper. Although the approach for meta learning in the paper was slightly different from the conventional learning to learn design,
the higher level idea is the same. Instead of porpagating updates, the optimizer is trained in a way to generate a whole new set of parameters for the next iterations of the optimizee. Their paper implements a genric image classification 
CNN, which performs well by learning from very few examples of a given class by generalizing well on the mini imagenet dataset.

We tried to extend the given implementation of learning to learn, use the conventional update generation model and train an image classifier. We continued to use te cifar image data set as in the parent paper.
We inferred that the over a period of 1000 epochs, the loss decreases and learns the parameters for the CNN. To run the code, follow the steps below.

##Steps:

1. If you are using sonnet version 0.16, you need to change the default parameter value of is_Training in the _build function in sonnet\python\modules\nets\convnet.py, from None to True.
_build(self, inputs, is_training=True, test_local_stats=True). This is to enable BatchNormalization.
2. Run the command python train.py --problem=fewshot --save_path=./fewshot



